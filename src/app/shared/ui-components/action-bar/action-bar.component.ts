import { Component, OnInit, Input } from '@angular/core';
import { isAndroid } from "tns-core-modules/platform";
import { RouterExtensions } from 'nativescript-angular/router';
import { Page } from 'tns-core-modules/ui/page/page';
import { UIService} from '../../services/ui-service';

declare var android: any;

@Component({
  selector: 'ns-action-bar',
  templateUrl: './action-bar.component.html',
  styleUrls: ['./action-bar.component.css'],
  moduleId: module.id,
})
export class ActionBarComponent implements OnInit {

    @Input() title:string;
    @Input() showBackButton:boolean = true;
    @Input() hideToggle=false;


  constructor(
      private page: Page ,
      private router: RouterExtensions,
      private uiservice: UIService
      ) { }

  ngOnInit() {
  }

  get android(){
      return isAndroid;
  }
  get canGoBack(){
    return this.router.canGoBack();
  }
  onGoBack(){
      this.router.backToPreviousPage();
  }

  onLoadedActionBar(){

    if(isAndroid){
        const androidToolbar = this.page.actionBar.nativeView;
        const backButton = androidToolbar.getNavigationIcon();
        if(backButton){
            backButton.setColorFilter(
                android.graphics.Color.parseColor('#171717'),
                (<any>android.graphics).PorterDuff.Mode.SRC_ATOP
            );
        }
    }
  }


  onToggleMenu(){
      this.uiservice.toggleDrawer();
  }

}
