import { Injectable }  from '@angular/core';

@Injectable({providedIn:'root'})
export class GlobalService{

    public Server:string = 'https://livfit.providerdom.com';
    public ApiUrl:string = '/api';
    public server_url = this.Server + this.ApiUrl;

    //Urls
    public Login:string = '/user/authentication';
    public getProfile:string = '/profile/get';

}
