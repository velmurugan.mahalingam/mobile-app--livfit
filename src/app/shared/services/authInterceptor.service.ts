import { Injectable, Inject } from '@angular/core';
import { HttpRequest, HttpHandler, HttpEvent, HttpInterceptor } from '@angular/common/http';
import { Observable } from 'rxjs';
import { setString, getString, hasKey, remove} from 'tns-core-modules/application-settings';
import { AuthService } from './authentication.service';

@Injectable()
export class AuthInterceptor implements HttpInterceptor{

    constructor(private auth_serv:AuthService){}

    intercept(request:HttpRequest<any>, next:HttpHandler):Observable<HttpEvent<any>>{
        console.log('Interceptor');
       if(hasKey('token')){
            const token = JSON.parse(getString('token'));
            console.log(token);
            if(token){
                request = request.clone({

                    setHeaders: { Authorization: `${token}` }

                });
            }
        }
        console.log(request);
        return next.handle(request);
    }
}
