import { Injectable }  from '@angular/core';
import { SnackBar } from "nativescript-snackbar";


@Injectable({providedIn:'root'})
export class AlertService{


public simple_alert(message){
    (new SnackBar()).simple(message);
}

}
