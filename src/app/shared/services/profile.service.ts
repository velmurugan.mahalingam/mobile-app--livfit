import { Injectable } from '@angular/core';
import { HttpHeaders,HttpClient, HttpErrorResponse } from '@angular/common/http';
import { GlobalService } from '~/app/shared/services/globals.service';
import { Observable, throwError } from 'rxjs';
import { map,catchError } from 'rxjs/operators';
import { User } from '~/app/provider-app/components/models/userProfile';
import { setString, getString, hasKey, remove} from 'tns-core-modules/application-settings';


@Injectable({ providedIn:'root'})
export class ProfileService{

    private userProfile:User;

    constructor(
         private http:HttpClient,
         private globals:GlobalService) { }


    getProfile(){
        return this.http.get(this.globals.server_url + this.globals.getProfile)
        .pipe(
        map((response:Response) => {
            this.userProfile = response['content']['profile'];
            this.userProfile['userEmailAddress'] = response['content']['user']['userEmailAddress'];
            this.userProfile['userMobileNumber'] = response['content']['user']['userMobileNumber'];
            this.userProfile['userName'] = response['content']['user']['userName'];
            this.userProfile['userType'] = response['content']['user']['userType'];
            this.userProfile['emailVerified'] = response['content']['profile']['emailVerified'];
            this.userProfile['phoneVerified'] = response['content']['profile']['phoneVerified'];
            this.userProfile['height'] = response['content']['profile']['height'];
            this.userProfile['weight'] = response['content']['profile']['weight'];
            this.userProfile['membership'] = response['content']['profile']['membership'];
            this.userProfile['organization'] = response['content']['profile']['organization'];
            return this.userProfile;

        }),catchError(error_response =>{
                this.handleError(error_response);
                return throwError(error_response);
        }));
    }

     private handleError(error:HttpErrorResponse){

       console.log(error);
    }

}
