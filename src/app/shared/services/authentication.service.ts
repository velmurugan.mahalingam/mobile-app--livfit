import { Injectable } from '@angular/core';
import { HttpHeaders,HttpClient, HttpErrorResponse } from '@angular/common/http';
import { RouterExtensions } from 'nativescript-angular/router';
import { UtilsService } from './utility-service';
import { GlobalService } from '~/app/shared/services/globals.service';
import { Observable, throwError, BehaviorSubject, of } from 'rxjs';
import { map,catchError, tap } from 'rxjs/operators';
import { AlertService } from './alert.service';
import { UserProfile } from '~/app/provider-app/components/models/userProfile';
import { setString, getString, hasKey, remove} from 'tns-core-modules/application-settings';


@Injectable({ providedIn:'root'})
export class AuthService{

   private token:string;
   private _user = new BehaviorSubject<UserProfile>(null);

   constructor(
        private router:RouterExtensions,
        private http:HttpClient,
        private utility:UtilsService,
        private globals:GlobalService,
        private alert:AlertService) { }


    get user(){
        return this._user.asObservable();
    }

    authentication(email:string,password:string):Observable<any>{

     this.token = 'Basic '+this.utility.base64Encode(email+":"+password);

    let httpOptions = {
        headers:new HttpHeaders({
            authorization: this.token
        })
    }
    console.log('Basic '+this.utility.base64Encode(email+":"+password));
    return this.http.get(this.globals.server_url + this.globals.Login, httpOptions)
    .pipe(
      catchError(error_response =>{
            this.handleError(error_response);
            return throwError(error_response);
        }),tap(resData => {
              if(resData.content.authenticated){
                console.log("Testing");
                console.log(this.token);
                this.handleLogin(resData.content.profile);
            }
        })
    );
}

autoLogin(){
    console.log("Auto Login");
    if(!hasKey('userData')){
        return of(false);
    }

    const token = JSON.parse(getString('token'));
    const userData = JSON.parse(getString('userData'));

    const loadedUser = new UserProfile(userData,token);

    if(loadedUser.is_auth){
        this._user.next(loadedUser);
        this.router.navigate(['/home'], { clearHistory:true} );
        return of(true);
    }
    return of(false);
}

exit(){
    this._user.next(null);
    remove('token');
    remove('userData');
    this.router.navigate(['/'],{ clearHistory: true});
}

private handleLogin(_profile){
    console.log(_profile);
    const user = new UserProfile(_profile,this.token);
    setString('token', JSON.stringify(this.token));
    setString('userData', JSON.stringify(user));
    this._user.next(user);

}
private handleError(error:HttpErrorResponse){

    switch(error.statusText){
        case 'Unauthorized':
              this.alert.simple_alert("Invalid Credentials!!");
                break;
        default:
                console.log(error);
                break;
    }
}



}
