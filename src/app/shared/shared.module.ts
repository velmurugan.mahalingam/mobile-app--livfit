import { NgModule, NO_ERRORS_SCHEMA } from '@angular/core';
import { NativeScriptCommonModule } from 'nativescript-angular/common';
import { ActionBarComponent } from './ui-components/action-bar/action-bar.component';
import { NativeScriptFormsModule } from 'nativescript-angular/forms';
import { ReactiveFormsModule } from '@angular/forms';
import { NativeScriptHttpClientModule } from "nativescript-angular/http-client";

@NgModule({
  declarations: [ActionBarComponent],
  imports: [
    NativeScriptCommonModule,
    NativeScriptFormsModule,
    NativeScriptHttpClientModule,
    ReactiveFormsModule
  ],
  exports:[
    NativeScriptCommonModule,
    NativeScriptFormsModule,
    NativeScriptHttpClientModule,
    ReactiveFormsModule,
    ActionBarComponent
  ],
  schemas: [NO_ERRORS_SCHEMA]
})
export class SharedModule { }
