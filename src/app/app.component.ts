import { Component, OnInit, OnDestroy, ViewChild, AfterViewInit, ChangeDetectorRef } from "@angular/core";
import { UIService } from "./shared/services/ui-service";
import { Subscription } from "rxjs";
import { RadSideDrawerComponent } from "nativescript-ui-sidedrawer/angular/side-drawer-directives";
import { RadSideDrawer } from "nativescript-ui-sidedrawer";
import { RouterExtensions } from "nativescript-angular/router/router-extensions";
import { AuthService } from "./shared/services/authentication.service";

@Component({
    selector: "ns-app",
    moduleId: module.id,
    templateUrl: "./app.component.html"
})
export class AppComponent implements OnInit,AfterViewInit,OnDestroy{

@ViewChild(RadSideDrawerComponent,{ static:false }) drawerComponent: RadSideDrawerComponent;
private drawerSub : Subscription;
private drawer:RadSideDrawer;
constructor(private ui_service: UIService,
            private changeDetectionRef:ChangeDetectorRef,
            private router:RouterExtensions,
            private authService:AuthService
    ){}

ngOnInit(){
    this.drawerSub = this.ui_service.drawerState.subscribe(() => {
        if(this.drawer){
            this.drawer.toggleDrawerState();
        }
    });
    this.authService.autoLogin().subscribe(success => {
        console.log("App Auto Login");
        console.log(success);
    });
}

ngAfterViewInit(){
    this.drawer = this.drawerComponent.sideDrawer;
    this.changeDetectionRef.detectChanges();
}

ngOnDestroy(){
    if(this.drawerSub){
        this.drawerSub.unsubscribe();
    }
}

logout(){
    if(this.drawerSub){
        this.drawer.toggleDrawerState();
    }
    this.authService.exit();
}

}
