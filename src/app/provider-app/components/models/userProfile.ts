export interface User{
    userId:number,
    userName:string,
    userType:string,
    userEmail:string,
    userMobile:string,
    qualification:string,
    imageUrl:string,
    npiNumber:string,
    address:Address,
    firstName:string,
    lastName:string,
    middleName:string,
    suffix:null,
    membership:string,
    organization:string,
    gender:string,
    dob:string,
    age:string,
    userEmailAddress:string,
    userMobileNumber:string,
    emaiVerified:boolean,
    phoneVerfied:boolean,
    weight:string,
    height:string

}

export interface Address{

    addressLine1:string;
    addressLine2:string;
    city:string;
    state:string;
    country:string;
    zipcode:string;

}

export class UserProfile{

    constructor(private userprofile:User,private _token:string){
        this.userprofile = userprofile;
        this._token = _token;
}

get is_auth(){
    return !!this._token;
}

get token(){
    if(!this._token){
        return null;
    }
    return this._token;
}

}
