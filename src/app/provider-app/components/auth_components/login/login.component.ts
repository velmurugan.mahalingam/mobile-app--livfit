import { Component, OnInit, ElementRef, ViewChild } from '@angular/core';
import { RouterExtensions } from 'nativescript-angular/router';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { TextField } from 'tns-core-modules/ui/text-field';
import { SnackBar } from "nativescript-snackbar";
import { AuthService } from '~/app/shared/services/authentication.service';
import { AlertService } from '~/app/shared/services/alert.service';



@Component({
  selector: 'ns-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css'],
  moduleId: module.id,
})
export class LoginComponent implements OnInit {

  form:FormGroup;
   isLoading = false;
  emailControlIsValid = true;
  passwordControlIsValid = true;
  @ViewChild('passwordEl' , {static :false }) passwordEl : ElementRef<TextField>;
  @ViewChild('emailEl' , {static :false }) emailEl : ElementRef<TextField>;

  constructor(private router:RouterExtensions,private authservice:AuthService,private alert:AlertService) { }

  ngOnInit() {

    this.form = new FormGroup({
        email : new FormControl(
              null , { updateOn : 'blur', validators : [ Validators.required,Validators.email,Validators.pattern('^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9]+.[a-zA-Z0-9-.]+$')]}
        ),
        password : new FormControl(
            null, { updateOn : 'blur' ,validators : [ Validators.required]}
        )
    });
    this.form.get('email').statusChanges.subscribe(status => {
        this.emailControlIsValid = status === 'VALID';
    });

    this.form.get('password').statusChanges.subscribe(status => {
        this.passwordControlIsValid = status === 'VALID';
    });
   }

  onSubmit(){
        this.passwordEl.nativeElement.focus();
        this.emailEl.nativeElement.focus();
        this.passwordEl.nativeElement.dismissSoftInput();
        const email = this.form.get('email').value;
        const password = this.form.get('password').value;
        if(email && password){
            if(this.form.valid){
                this.isLoading = true;
                this.authservice.authentication(email, password).subscribe(response => {
                    this.isLoading = false;
                    this.router.navigate(['/home'],{ clearHistory:true });
                },(err) => {
                    this.isLoading = false;
                     console.log("Auth error");
                     console.log(err);
                });
            }
        }else {
            this.alert.simple_alert("Email and Password Fields are Required!!");

        }

}

}
