import { NgModule, NO_ERRORS_SCHEMA } from '@angular/core';
import { NativeScriptCommonModule } from 'nativescript-angular/common';
import { HomeRoutingModule } from './home-routing.module';
import { SharedModule } from '~/app/shared/shared.module';


@NgModule({
  imports: [
    NativeScriptCommonModule,
    SharedModule,
    HomeRoutingModule
  ],
  declarations: [HomeRoutingModule.components],
  schemas: [NO_ERRORS_SCHEMA]
})
export class HomeModule { }
