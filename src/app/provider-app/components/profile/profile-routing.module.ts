import { NgModule } from "@angular/core";
import { NativeScriptRouterModule } from "nativescript-angular/router";
import { Routes } from "@angular/router";
import { ProfileTabComponent } from "./profile-tab/profile-tab.component";

const routes:Routes = [
     {
         path:'tabs',
         component:ProfileTabComponent,
     },
     {
        path:'',
        redirectTo:'/profile/tabs',
        pathMatch:'fulll'
     }
];

@NgModule({

    imports: [ NativeScriptRouterModule.forChild(routes)],
    exports:[NativeScriptRouterModule]
})
export class ProfileRoutingModule{

    static profile_components = [
        ProfileTabComponent
    ];

}
