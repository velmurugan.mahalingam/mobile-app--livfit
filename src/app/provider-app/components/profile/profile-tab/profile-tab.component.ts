import { Component, OnInit } from '@angular/core';
import { RouterExtensions } from 'nativescript-angular/router';
import { ActivatedRoute } from '@angular/router';
import { Page } from 'tns-core-modules/ui/page/page';
import { User } from '../../models/userProfile';
import { ProfileService } from '~/app/shared/services/profile.service';
import { isEmpty } from 'rxjs/operators';

@Component({
  selector: 'ns-profile-tab',
  templateUrl: './profile-tab.component.html',
  styleUrls: ['./profile-tab.component.css'],
  moduleId: module.id,
})
export class ProfileTabComponent implements OnInit {

    userProfile:User;

    constructor(private _profile_service:ProfileService) { }

    ngOnInit() {
        this.get_profile();
    }

   get_profile(){
      this._profile_service.getProfile().subscribe( (data:User) => {
         if(!this.isEmpty(data)){
            this.userProfile = data;
            console.log(data);
         }
      });
    }

    isEmpty(obj){
        for(const key in obj){
            if(obj.hasOwnProperty(key)){
                return false;
            }
        }
        return true;
    }
}
