import { NgModule, NO_ERRORS_SCHEMA } from '@angular/core';
import { NativeScriptCommonModule } from 'nativescript-angular/common';
import { SharedModule } from '~/app/shared/shared.module';
import { ProfileRoutingModule } from './profile-routing.module';

@NgModule({
  imports: [
                NativeScriptCommonModule,
                SharedModule,
                ProfileRoutingModule
            ],
declarations: [
                ProfileRoutingModule.profile_components

            ],
  schemas: [NO_ERRORS_SCHEMA]
})
export class ProfileModule { }
